FROM tomcat:8

COPY target/${JAR_FILE}  $CATALINA_HOME/webapps
COPY docker/redisson-all-3.11.2.jar $CATALINA_HOME/lib
COPY docker/redisson-tomcat-8-3.11.2.jar $CATALINA_HOME/lib
COPY docker/context.xml $CATALINA_HOME/conf
COPY docker/web.xml $CATALINA_HOME/conf
COPY docker/redisson.yaml $CATALINA_HOME

