# Scaling a Tomcat/Wicket App with Redis

Example to store the servlet session from Tomcat externaly in Redis.

## Goal

We want that the ServletSessions survives server crashes/restarts and we want to have an easy way for loadbalancing without sticky sessions. We also want update the app and keep the ServletSessions alive.

## What do we need?

* Maven
* Docker
* docker-compose

## The app

The App is a simple counter. Every page reload increments the counter. The 'click me'-Link do this via AJAX.
The data of the counter is stored in a Wicket-Model which is stored in the ServletSession:

File: [HomePage.java](src/main/java/de/evodion/HomePage.java)
```java
        ...
		Model<Integer> model = new Model<Integer>() {
			private int counter = 0;
			public Integer getObject() {
				return counter++;
			}
		};
		final Label label = new Label("counter", model);
		label.setOutputMarkupId(true);
		add(label);
		...
```

## Test the app without Redis

* Import the maven project in your favorite IDE and start the main method in the class 'Start'.
* Visit http://localhost:8080 and click 'click me' a few times.
* Restart the app and click again -> the counter should be reseted and start again from zero. That means the Session was lost.

## Test the app with Redis

* Build the project with maven:

        mvn clean package

* Start app stack

        docker-compose up -d

* View the application logs und wait until the tomcat server is started (hit CRTL-C for exit)

        docker-compose logs -f

* Visit the app at http://localhost:8081/app and click 'click me' a few times

* Visit the redis commander at http://localhost:8090 to show the content of the Redis instance. You should see a key for the ServletSession.

* Now kill the tomcat Server and start it again:

        docker-compose kill tomcat
        docker-compose up -d

* Click 'click me' again. The counter should not be reseted.

* You can also destroy the hole app stack and restart:

        docker-compose down
        docker-compose up -d

* The ServletSession should be alive, because the Redis-Cache is persistet in a docker volume.

## How it works

### Redisson session manager

The magic happens completly outside of the app. It is a special Tomcat session manager configuration:

File: [context.xml](docker/context.xml) (placed in ```$CATALINA_HOME/conf```)

````xml
<Context>
    <Manager className="org.redisson.tomcat.RedissonSessionManager"
             configPath="${catalina.base}/redisson.yaml"
             readMode="REDIS" updateMode="DEFAULT" broadcastSessionEvents="false"/>

</Context>
````

Links:
* [Redis - in memory store](https://redis.io/)
* [Redisson - Java Redis client](https://github.com/redisson/redisson)
* [Redisson configuration](https://github.com/redisson/redisson/wiki/2.-Configuration)
* [Redis based Tomcat Session Manager](https://github.com/redisson/redisson/tree/master/redisson-tomcat)

### Session Timeout

The session invalidation per timout is performed via the TTL function from Redis. The timeout can be configured via the ````web.xml```` an usual:

File: [web.xml](docker/web.xml) (placed in ```$CATALINA_HOME/conf```)

````xml
  <!-- ==================== Default Session Configuration ================= -->
  <!-- You can set the default session timeout (in minutes) for all newly   -->
  <!-- created sessions by modifying the value below.                       -->

    <session-config>
        <session-timeout>2</session-timeout>
    </session-config>

````



