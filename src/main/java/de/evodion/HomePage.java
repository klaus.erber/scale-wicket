package de.evodion;


import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;


public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;

	public HomePage(final PageParameters parameters) {
		super(parameters);



		final Label label = new Label("counter", createCounterModel());
		label.setOutputMarkupId(true);
		add(label);

		add(new AjaxLink<Void>("link") {
			public void onClick(AjaxRequestTarget target) {
				if (target != null) {
					// target is only available in an Ajax request
					target.add(label);
				}
			}
		});

		add(new Label("ip", createIpModel()));

	}

	private IModel<Integer> createCounterModel() {
		return new Model<Integer>() {
				private int counter = 0;
				public Integer getObject() {
					return counter++;
				}
			};
	}

	private IModel<String> createIpModel() {
		return new Model<String>() {
			String ip;

			@Override
			public String getObject() {
				if (ip == null) {
					try {
						ip = InetAddress.getLocalHost().toString();
					} catch (UnknownHostException e) {
						ip = "UNKNOWN";
					}
				}
				return ip;
			}
		};
	}
}
